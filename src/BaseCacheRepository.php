<?php

namespace SWC\BaseRepo;

use Illuminate\Support\Facades\Cache;
use SWC\BaseRepo\Contracts\BaseRepository;

abstract class BaseCacheRepository implements BaseRepository 
{
	public function getById($id, $attributes = array('*'))
	{
		return Cache::tags([get_class($this->repo), 'get_by_id'])->remember(md5($id .'.'. implode('|', $attributes)), 10*60, function() use ($id, $attributes) {
			return $this->repo->getById($id, $attributes);
		});
	}

	public function getByIdWith($id, $relations = null, $attributes = array('*'))
	{
		$relations = is_array($relations) ? $relations : (isset($relations) ? [$relations] : []);
		return Cache::tags([get_class($this->repo), 'get_by_id_width'])->remember(md5($id .'.'. implode('|', $relations) .'.'. implode('|', $attributes)), 10*60, function() use ($id, $relations, $attributes) {
			return $this->repo->getByIdWith($id, $relations, $attributes);
		});
	}

	public function getAll($attributes = array('*'), $offset = null, $limit = 10)
	{
		return Cache::tags([get_class($this->repo), 'get_all'])->remember(md5(implode('|', $attributes) .'.'. $offset .'.'. $limit), 10*60, function() use ($attributes, $offset, $limit) {
			return $this->repo->getAll($attributes, $offset, $limit);
		});
	}

	public function update($id, array $attributes)
	{
		return $this->repo->update($id, $attributes);
	}

	public function create(array $attributes)
	{
		return $this->repo->create($attributes);
	}

	public function delete($ids)
	{
		return $this->repo->delete($ids);
	}

	public function find($id, $columns = array('*'))
	{
		return Cache::tags([get_class($this->repo), 'find'])->remember(md5($id .'.'. implode('|', $columns)), 10*60, function() use ($id, $columns) {
			return $this->repo->find($id, $columns);
		});
	}

	public function all($columns = array('*'))
	{
		return Cache::tags([get_class($this->repo), 'all'])->remember(md5(implode('|', $columns)), 10*60, function() use ($columns) {
			return $this->repo->all($columns);
		});
	}
}

?>