<?php

namespace SWC\BaseRepo;

use Illuminate\Database\Eloquent\Model;
use SWC\BaseRepo\Contracts\BaseRepository;

abstract class BaseEloqRepository implements BaseRepository 
{
	protected $model;

	public function __construct(Model $model)
	{
		$this->model = $model;
	}

	public function getById($id, $attributes = array('*'))
	{
		// $id is a business key -> fetch the object by its bkey | if not string fetch it by its surrogate (int) key
		if(!is_numeric($id)) {
			return $this->model->where('bkey', $id)->first($attributes);
		}
		return $this->model->find($id, $attributes);
	}

	public function getByKeys(array $key, $attributes)
	{
		$query = $this->model->whereIn('bkey', $id);

		return $query->get($attributes);
	}

	public function getByIdWith($id, $relations = null, $attributes = array('*'))
	{
		// $id is an array of identifiers -> fetch all objects
		if(is_array($id)) {
			// $id is a business key -> fetch the object by its bkey | if not string fetch it by its surrogate (int) key
			$query = (!is_numeric($id)) ? $this->model->whereIn('bkey', $id) : $this->model->whereIn('id', $id);
		}else{
			$query = (!is_numeric($id)) ? $this->model->where('bkey', $id) : $this->model->where('id', $id);
		}

        if(isset($relations)) {
        	$query->with($relations);
        }

        if(is_array($id)) {
        	return $query->get($attributes);
        }
        return $query->first($attributes);
	}

	public function getAll($attributes = array('*'), $offset = null, $limit = 10)
	{
		$query = $this->model;

		if(isset($offset)) {
			return $query->offset($offset)->limit($limit)->get();
		}
		return $query->all($attributes);
	}

	public function update($id, array $attributes)
	{
		return $this->model->find($id)->update($attributes);
	}

	public function create(array $attributes)
	{
		return $this->model->create($attributes);
	}

	public function firstOrCreate(array $attributes, array $values = array())
	{
		return $this->model->firstOrCreate($attributes, $values);
	}

	public function updateOrCreate(array $attributes, array $values = array())
	{
		return $this->model->updateOrCreate($attributes, $values);
	}

	public function delete($ids)
	{
		return $this->model->destroy((is_array($ids) ? $ids : array($ids)));
	}

	//////////////////////////////////////
	// Old functions, should be cleared //
	//////////////////////////////////////
	public function find($id, $columns = array('*'))
	{
		return $this->model->find($id, $columns);
	}

	public function all($columns = array('*'))
	{
		return $this->model->all($columns);
	}
}

?>