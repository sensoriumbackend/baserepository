<?php

namespace SWC\BaseRepo\Contracts;

interface BaseRepository 
{

	public function getById($id, $attributes = array('*'));

	public function getByIdWith($id, $relations = null, $attributes = array('*'));

	public function getAll($attributes = array('*'), $offset = null, $limit = 10);

	public function update($id, array $attributes);

	public function create(array $attributes);

	public function delete($ids);

	//////////////////////////////////////
	// Old functions, should be cleared //
	//////////////////////////////////////
	public function find($id, $columns = array('*'));

	public function all($columns = array('*'));

}


?>